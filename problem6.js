// 1² + 2² + ... 10² = algo;
// (1+ 2... +10)² = algo;
function num(end) {
  const numbers = []
  for (let index = 0; index <= end; index++) {
    numbers.push(index)
  }
  let sumOfAllSquare = numbers.reduce((p, c) => { return p + (Math.pow(c, 2)) })
  let sumOfAll = numbers.reduce((p, c) => { return p + c})
  sumOfAll = Math.pow(sumOfAll, 2)
  return sumOfAll - sumOfAllSquare
}