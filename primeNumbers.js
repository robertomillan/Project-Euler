let fs = require('fs')
function Prime(stop){
    for (let i = 2; i < stop; i++) {
        if (stop % i === 0) {return false}
    }
    return stop > 1
}
const numbers = []
function pushNumber(stop) {
    for (let i = 0; i <= stop; i++) {
        if (Prime(i) == !0) {
            numbers.push(i)
        }
    }
}
pushNumber(2e6)
fs.writeFile('prime.json', JSON.stringify(numbers), (err)=> {
    if(err){
        console.log(err)
    }
    console.log('saved correctly')
})
