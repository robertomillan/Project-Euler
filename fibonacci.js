function fibonacci (stop) {
  let a = 1, b = 0, c, storage = []
  for (let i = 0; a < stop; i++) {
    c = a
    a = a + b
    b = c
    if (b % 2 === 0) {
      let even = b
      storage.unshift(even)
    }
  }
  var all = storage.reduce((x, y) => { return x + y }, 0)
  return all
}
fibonacci(4e6)
