// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143 ?

// fs.writeFile('prime.json', JSON.stringify(primeNumbers), (err)=> {
//     if(err){
//         console.log(err)
//     }
//     console.log('saved correctly')
// })

// const numbers = require('./prime.json')
// const fs = require('fs')

function primeNumber(a) {return 2 === a || 3 === a || 5 === a || 7 === a || (0 == a%2 || 0 == a%3 || 0 == a%5 || 0 == a%7 || 1 == a ? !1 : !0)}

function PrimeFactors(num){
  let number = num
  const numbers = []
  function pushNumber(){
    for(let i = 0; i <= 9e5; i++){
      if(primeNumber(i) == !0){
        numbers.push(i)
      }
    }
  }
  pushNumber()
  function filterNumbers(num){
    let number = num, result = [], primes = numbers
    for(let i = 0; primes[i] >= 0; i++){
      if (number < primes[i]) {break}
      if (number % primes[i] === 0 && number >= 0) {
        do {
          number /= primes[i]
          result.push(primes[i])
        } while (number % primes[i]===0 && primes[i] != 1);
      }
    }
    console.log(result)
  }
  filterNumbers(num)
}
PrimeFactors(28402010)






